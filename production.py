# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval
from decimal import Decimal

__all__ = ['Production']


class Production(metaclass=PoolMeta):
    __name__ = 'production'

    @classmethod
    def __setup__(cls):
        super(Production, cls).__setup__()
        cls._buttons.update({
            'split': {
                'readonly': Eval('state').in_(['running', 'done']),
                },
            })

    def split(self, quantity, uom, count=None):
        if self.number == " ":
            pool = Pool()
            Sequence = pool.get('ir.sequence')
            Configuration = pool.get('production.configuration')
            config = Configuration(1)
            self.number = Sequence.get_id(
                config.production_sequence.id)
            self.save()

        productions = super(Production, self).split(quantity, uom, count=count)
        if self.purchase_request:
            for production in productions:
                if not production.purchase_request:
                    production.purchase_request = self.purchase_request

                if production.destination_warehouse:
                    from_location = production.warehouse.storage_location
                    to_location = production.destination_warehouse.storage_location if production.destination_warehouse.storage_location else production.destination_warehous
                    if production.incoming_shipment:
                        if production.incoming_shipment.moves:
                            Pool().get('stock.move').delete(production.incoming_shipment.moves)
                        shipment = production.incoming_shipment
                    else:
                        shipment = Pool().get('stock.shipment.internal')()
                        shipment.from_location = from_location
                        shipment.to_location = to_location

                    shipment.reference = production.number
                    shipment.moves = []
                    for output in production.outputs:
                        move = production._get_incoming_shipment_move(output, from_location, to_location)
                        shipment.moves += (move,)
                    shipment.save()
                    production.incoming_shipment = shipment

                    Pool().get('stock.shipment.internal').wait([production.incoming_shipment])
                production.save()

    # We need it to calculate the products manufactured externally
    def get_subcontracted_service_cost(self):
        Uom = Pool().get('product.uom')
        cost = Decimal(0)
        if self.purchase_request:
            pr = self.purchase_request
            if pr.purchase_line:
                quantity = Uom.compute_qty(self.uom, self.quantity,
                    self.product.default_uom)
                cost = Decimal(str(quantity)) * pr.purchase_line.unit_price

        digits = self.__class__.cost.digits
        return cost.quantize(Decimal(str(10 ** -digits[1])))

    # Override Production.get_cost adding the subcontracted_service_cost
    def get_cost(self, name):
        cost = super(Production, self).get_cost(name)
        if self.purchase_request:
            cost += self.get_subcontracted_service_cost()
        return cost

    # Override Production.on_change_with_cost adding the subcontracted_service_cost
    @fields.depends('inputs', 'purchase_request', 'product')
    def on_change_with_cost(self):
        cost = super(Production, self).on_change_with_cost()
        if self.product and self.purchase_request:
            cost += self.get_subcontracted_service_cost()
        return cost
